/*COMANDOS PARA EJECUTAR EL SERVER
Usando la shell: nodemon serv.js; ./node_modules/.bin/nodemon serv.js;
Si se agrega como script en el package.json:
{
    "scripts":{
        "dev": "nodemon serv.js"
    }
}
Se correria en shell asi: npm run dev
*/

//Declaracion de variables
const express = require('express');
const bodyPars = require('body-parser');
const mongo = require('mongodb').MongoClient;
const ObjectId = require('mongodb').ObjectID;
const app = express();

//Utilidades
app.use(bodyPars.urlencoded({ extended: true }));
app.set('view engine', 'ejs');
app.use(bodyPars.json());

//Conexion a la base de datos
var datab, url = 'mongodb://localhost:27017/aficionesdb';

mongo.connect(url, (err, database) => {
    if (err) return console.log(err);
    datab = database.db('aficionesdb');

    //Creamos el server aca como tecnica de conexion en el instante de crear la mongodb
    app.listen(7000, () => {
        console.log('Server corriendo en puerto: 7000');
    });
});

//Operaciones basicas
//Registrar
app.post('/regis', (req, res) => {
    datab.collection('Aficion').insertOne(req.body, (error, resp) => {
        if (error) return console.log(error);

        //Nos dirigimos a la ruta base del proyecto
        console.log('Registro exitoso');
        res.redirect('/');
    });
});

//Consultar
app.get('/', (req, res) => {
    datab.collection('Aficion').find().toArray(function (error, resp) {
        //impresion y envio de datos
        if (error) return console.log(error);

        //console.log(resp);
        console.log('Consulta exitosa');
        res.render('index.ejs', { aficiones: resp });
    });
});

//Consultar por id
app.get('/edit/:id', (req, res) => {
    datab.collection('Aficion').findOne({ _id: ObjectId(req.params.id) }, function (error, resp) {
        //impresion y envio de datos
        if (error) return console.log(error);

        console.log('Envio a edicion');
        res.render('edite.ejs', { doc: resp });
    });
});

//Actualizar
app.post('/update', (req, res) => {
    datab.collection('Aficion').findOneAndUpdate({ _id: ObjectId(req.body.id) }, {
        $set: {
            nombre: req.body.nombre,
            descripcion: req.body.descripcion
        }
    }, {
        sort: { _id: -1 },
        upsert: true
    }, (err, resp) => {
        if (err) return console.log(err);

        console.log("Actualizado con exito");
        res.redirect("/");
    });
});

//borrar
app.get('/delete/:id', (req, res) => {
    datab.collection('Aficion').findOneAndDelete({ _id: ObjectId(req.params.id) }, (err, resp) => {
        if (err) return console.log(err);

        console.log("Eliminado de almacen");
        res.redirect("/");
    });
});